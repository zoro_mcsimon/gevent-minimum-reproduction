# Use the official lightweight Python image.
# https://hub.docker.com/_/python
FROM python:3.7-slim

# Copy local code to the container image.
ENV APP_HOME /app
WORKDIR $APP_HOME
COPY . ./

# Install production dependencies.
RUN pip install Flask gunicorn
RUN pip install gunicorn[gthread]
RUN pip install google-cloud-firestore

ARG google_creds=/app/MichaelSimon.json
ARG google_project=zoro-michael-simon
ENV GOOGLE_APPLICATION_CREDENTIALS=$google_creds
ENV GCLOUD_PROJECT=$google_project

# Run the web service on container startup. Here we use the gunicorn
# webserver, with one worker process and 8 threads.
# For environments with multiple CPU cores, increase the number of workers
# to be equal to the cores available.
CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 --worker-class gthread app:app
